README
======

## Assignment 1
Creating a blog app. 
Used Post and Comment in the model.

## Assignment 2
Added associations and validations.

## Assignment 3
Added comments view/add to posts view, added HTTP Basic auth.

## Assignment 4
Added ajax support for adding a new comment.

---

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
`<tt>rake doc:app</tt>`.


